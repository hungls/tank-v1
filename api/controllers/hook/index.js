module.exports = {


    friendlyName: 'Index',


    description: 'Index hook.',


    inputs: {
        message: {
            type: 'string',
            required: true
        },
        user: {
            type: 'string',
            required: true
        },
        streamId: {
            type: 'string',
            required: true
        }
    },


    exits: {

    },


    fn: async function (inputs) {
        var action;
        switch (inputs.message) {
            case '#create':
                action = 'tank-create';
                let uid = inputs.user;
                let x = await sails.helpers.getRandomArbitrary.with({ min: 40, max: (sails.config.globals.games.MAP_WIDTH - 100) });
                let y = await sails.helpers.getRandomArbitrary.with({ min: 40, max: (sails.config.globals.games.MAP_HEIGHT - 100) });

                let tank = {
                    'uid': uid,
                    'name': uid,
                    'x': x,
                    'y': y,
                    "orient": 1
                };

                //add user to array
                sails.config.globals.games.playerTank.push(tank);

                let dataNewGame = {
                    'tank': sails.config.globals.games.playerTank,
                    'bullet': sails.config.globals.games.bulletArr
                };
                await sails.sockets.broadcast(inputs.streamId, 'user', dataNewGame, this.req);
                await sails.sockets.broadcast(inputs.streamId, 'best_score', sails.config.globals.games.bestScoreArr, this.req);
                await sails.sockets.broadcast(inputs.streamId, 'new_enemy', tank, this.req);

                console.log('user connected: ' + uid);
                break;
            case '#up':
                action = 'tank-move';
                break;
            case '#down':
                action = 'tank-move';
                break;
            case '#left':
                action = 'tank-move';
                break;
            case '#right':
                action = 'tank-move';
            case '#shoot':
                action = 'tank-shoot'
                break;
            default:
                break;
        }
        if (action) sails.sockets.broadcast(inputs.streamId, action, inputs, this.req);
        // All done.
        return;

    }


};
