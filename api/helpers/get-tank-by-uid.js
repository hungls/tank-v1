module.exports = {


    friendlyName: 'Get tank by uid',


    description: '',


    inputs: {
        uid: {
            type: 'string',
            required: true,
        },
        roomId: {
            type: 'string',
            required: true,
        }
    },


    exits: {

        success: {
            outputFriendlyName: 'Tank by uid',
        },

    },


    fn: async function (inputs) {
        if (!sails.config.globals.games.playerTank[inputs.roomId]) return null;
        let playerTank = sails.config.globals.games.playerTank[inputs.roomId];
        for (let i = 0; i < playerTank.length; i++) {
            if (playerTank[i]["uid"] == inputs.uid) {
                return playerTank[i];
            }
        }
        return null;
    }


};

