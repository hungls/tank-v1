module.exports = {


    friendlyName: 'User die',


    description: '',


    inputs: {
        roomId: {
            type: 'string',
            required: true
        },
        tank: {
            type: 'json',
            required: true
        },
        bullet: {
            type: 'json',
            required: true
        }
    },


    exits: {

    },


    fn: async function (inputs, exits) {
        let newTankList = sails.config.globals.games.playerTank[inputs.roomId].filter(object => {
            return object.uid !== inputs.tank.uid;
        });
        sails.config.globals.games.playerTank[inputs.roomId] = newTankList;

        let attacker = `${inputs.bullet.uid}`;
        let bulletId = `${inputs.bullet.id}`;
        let shoot = {};
        shoot[bulletId] = {
            victim: inputs.tank.uid,
            attackerName: inputs.bullet.uname
        };

        (sails.config.globals.games.bestScoreArr[inputs.roomId][attacker])
            ? sails.config.globals.games.bestScoreArr[inputs.roomId][attacker][bulletId] = {
                victim: inputs.tank.uid,
                attackerName: inputs.bullet.uname
            }
            : sails.config.globals.games.bestScoreArr[inputs.roomId][attacker] = shoot;

        let sortedBestScore = await sails.helpers.calculateBestScore.with({ roomId: inputs.roomId });
        await sails.sockets.broadcast(inputs.roomId, 'best_score', sortedBestScore);

        await sails.sockets.broadcast(inputs.roomId, 'user_die_update', inputs);

        return exits.success({ inputs });
    }


};
