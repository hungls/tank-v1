const ShortUniqueId = require('short-unique-id');
const getBulletId = new ShortUniqueId({ length: 4 });

module.exports = {


    friendlyName: 'Shoot',


    description: 'Shoot hook.',


    inputs: {
        user: {
            type: 'string',
            required: true
        },
        roomId: {
            type: 'string',
            required: true,
        }
    },


    exits: {

    },


    fn: async function (inputs, exits) {
        let bulletSize = 8;
        let tankSize = 40;
        let bulletSpeed = 4;

        let uid = inputs.user;

        let tank = await sails.helpers.getTankByUid.with({ uid, roomId: inputs.roomId });
        if (!tank) return exits.success({ message: 'Tank not found' });

        let bulletId = getBulletId();

        let shoot = {
            "uid": uid,
            "id_bullet": bulletId,
            "x": tank.x + tankSize / 2 - bulletSize / 2,
            "y": tank.y + tankSize / 2 - bulletSize / 2,
            "orient": tank.orient,
            "speed": bulletSpeed,
            "uname": tank.name
        }

        await sails.sockets.broadcast(inputs.roomId, 'new_bullet', shoot);

        return exits.success({ shoot });
    }

};
