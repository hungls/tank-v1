# sails-tanks

a [Sails v1](https://sailsjs.com) application


### Links

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)

### Install
+ ```npm install```

### Run
+ ```sails lift```

+ Open browser to create room, eg: [http://localhost:1337/room/123456789](http://localhost:1337/room/123456789) with ```123456789``` is your room id

+ To join room ```curl --location 'http://localhost:1337/api/v1/hooks' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'user=hungls-tiktok-uid' --data-urlencode 'roomId=123456789' --data-urlencode 'name=Hưng'```

+ To move ```curl --location 'http://localhost:1337/api/v1/hooks/move' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'user=hungls-tiktok-uid' --data-urlencode 'roomId=123456789' --data-urlencode 'orient=4'``` with ```orient=1``` for up, ```orient=2``` for down, ```orient=3``` for left and ```orient=4``` for right

+ To shoot ```curl --location 'http://localhost:1337/api/v1/hooks/shoot' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'user=hungls-tiktok-uid' --data-urlencode 'roomId=123456789'```

### Version info

This app was originally generated on Fri Jun 09 2023 16:08:21 GMT+0700 (Indochina Time) using Sails v1.5.4.

<!-- Internally, Sails used [`sails-generate@2.0.8`](https://github.com/balderdashy/sails-generate/tree/v2.0.8/lib/core-generators/new). -->



<!--
Note:  Generators are usually run using the globally-installed `sails` CLI (command-line interface).  This CLI version is _environment-specific_ rather than app-specific, thus over time, as a project's dependencies are upgraded or the project is worked on by different developers on different computers using different versions of Node.js, the Sails dependency in its package.json file may differ from the globally-installed Sails CLI release it was originally generated with.  (Be sure to always check out the relevant [upgrading guides](https://sailsjs.com/upgrading) before upgrading the version of Sails used by your app.  If you're stuck, [get help here](https://sailsjs.com/support).)
-->

