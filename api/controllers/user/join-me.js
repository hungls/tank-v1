module.exports = {


    friendlyName: 'Join me',


    description: '',


    inputs: {
        roomId: {
            type: 'string',
            required: true
        }
    },


    exits: {
        invalid: {
            responseType: 'badRequest',
        },
    },


    fn: async function (inputs, exits) {

        try {
            // Make sure this is a socket request (not traditional HTTP)
            if (!this.req.isSocket) {
                throw 'invalid';
            }
            if (!sails.config.globals.games.playerTank[inputs.roomId]) sails.config.globals.games.playerTank[inputs.roomId] = [];
            if (!sails.config.globals.games.bulletArr[inputs.roomId]) sails.config.globals.games.bulletArr[inputs.roomId] = [];
            if (!sails.config.globals.games.bestScoreArr[inputs.roomId]) sails.config.globals.games.bestScoreArr[inputs.roomId] = [];
            let roomData = {
                playerTank: sails.config.globals.games.playerTank[inputs.roomId],
                bulletArr: sails.config.globals.games.bulletArr[inputs.roomId]
            }

            await sails.sockets.join(this.req, inputs.roomId);
            await sails.sockets.broadcast(inputs.roomId, 'join', roomData);
            let sortedBestScore = await sails.helpers.calculateBestScore.with({ roomId: inputs.roomId });
            await sails.sockets.broadcast(inputs.roomId, 'best_score', sortedBestScore);
            return exits.success({ status: 'success' });
        } catch (error) {
            throw 'invalid';
        }
    }

};
