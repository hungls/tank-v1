module.exports = {


    friendlyName: 'Find one',


    description: '',


    inputs: {
        id: {
            type: 'string',
            required: true
        }
    },


    exits: {

    },


    fn: async function (inputs) {
        // All done.
        return this.res.view('pages/homepage', {
            roomId: inputs.id
        });
    }


};
