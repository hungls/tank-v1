// 'use strict';

const MAP_WIDTH = 900;
const MAP_HEIGHT = 700;

var map;
let speed = 40;
let orient = 0;
var tankSize = 40;
var playerTankMgr = new TankManager();

var bulletMgr = new BulletManager();
var bulletSpeed = 4;
var bulletSize = 8;
var bulletID = 0;

var playerType = 1;
var enemyType = 2;

var isStart = false;

let context;

let countAnim = 0;
let animMgr = new AnimationManager();
let animTime = 3;

function addNewEnemyTank(enemyTank) {
    playerTankMgr.addNewTank(enemyTank);
}
function updateEnemyTank(x, y, orient, uid) {
    playerTankMgr.updateTank(x, y, orient, uid);
}
function removeEnemyTank(uid) {
    playerTankMgr.removeTank(uid);
}
function update_best_score(dataArr) {
    let html_best_score = '';
    for (let i in dataArr) {
        let usrs = i.split("::");
        let score = dataArr[i];
        html_best_score += '<div class="player ' + usrs[0] + '" data-id="' + usrs[0] + '">' +
            '<span class="name">' + usrs[1] + '</span>: ' +
            '<span class="score">' + score + '</span>' +
            '</div>';
    }
    $('.top10').html(html_best_score);
}

var socket = io.sails.connect();

socket.on('connect', async function onConnect() {
    await socket.get('/api/v1/users/join-me', { roomId: roomId });

    function explore(x, y, type) {
        let anim = new Animation(x, y, type);
        animMgr.addNewAnim(anim);
    }

    socket.on('join', function (response) {
        if (isStart == false) {
            isStart = true;
            map = new TankMap(MAP_WIDTH, MAP_HEIGHT, 20);
            response.playerTank.forEach(specs => {
                let id = specs.uid;
                let name = specs.name;
                let x = specs.x;
                let y = specs.y;
                let orient = specs.orient;

                let tank = new Tank(x, y, speed, enemyType, id);
                tank.currOrient = orient;
                tank.name = name;

                addNewEnemyTank(tank);
            });
        }
    });

    socket.on('new_enemy', function (response) {
        let id = response["uid"];
        let name = response["name"];
        let x = response["x"];
        let y = response["y"];
        let orient = response["orient"];

        let newEnemy = new Tank(x, y, speed, enemyType, id);
        newEnemy.currOrient = orient;
        newEnemy.name = name;
        addNewEnemyTank(newEnemy);
    });

    socket.on('player_move', function (response) {
        let id = response["uid"];
        let x = response["x"];
        let y = response["y"];
        let orient = response["orient"];

        updateEnemyTank(x, y, orient, id);

    });

    socket.on('new_bullet', function (response) {
        let uid = response["uid"];
        let id = response["id_bullet"];
        let x = response["x"];
        let y = response["y"];
        let orient = response["orient"];
        let uname = response["uname"];

        let newBullet = new Bullet(x, y, orient, bulletSpeed, 2, bulletSize, uid, uname);

        newBullet.setID(id);
        bulletMgr.addNewBullet(newBullet);
    });

    socket.on('user_die_update', function (response) {
        let uidEnemy = response.bullet.uid;
        let idBullet = response.bullet.id;

        bulletMgr.removeBullet(uidEnemy, idBullet);
        removeEnemyTank(response.tank.uid);
    });

    socket.on('best_score', function (best_score) {
        update_best_score(best_score);
    });


    function emitDie(tank, bullet) {
        let die = {
            roomId,
            tank,
            bullet
        };
        // socket.emit('user_die', die);
        socket.post('/api/v1/hooks/user-die', die, (res) => { console.log(res); });
    }

    function moveUniversal() {
        var tempBulletArr = new Array();
        for (var i = 0; i < bulletMgr.bulletArr.length; i++) {
            var bullet = bulletMgr.bulletArr[i];
            var isDestroyBullet = false;
            for (var j = 0; j < playerTankMgr.tankArr.length; j++) {
                let tank = playerTankMgr.tankArr[j];
                if (tank.uid != bullet.uid && bullet.type == enemyType && tank.isInside(bullet.x, bullet.y, bulletSize)) {
                    isDestroyBullet = true;

                    // isMovetable = false;
                    tank.isAlive = false;

                    explore(tank.x, tank.y, 1);
                    emitDie(tank, bullet);
                    break;
                } else if (map.isMoveTable(bullet.x, bullet.y, bulletSize) == false) {
                    isDestroyBullet = true;
                    break;
                }
            }
            if (isDestroyBullet == false) {
                bulletMgr.bulletArr[i].move();
                tempBulletArr.push(bullet);
            } else {
                explore(bullet.x, bullet.y, 2);
            }
        }
        bulletMgr.bulletArr = tempBulletArr;
    }

    function draw(context) {
        // xoa nen
        context.fillStyle = '#000000';
        context.fillRect(0, 0, 1440, 900);
        // draw obj
        map.draw(context);
        bulletMgr.drawAll(context);
        playerTankMgr.drawAll(context);

        // run anim
        countAnim++;
        if (countAnim > animTime) {

            // chay anim
            animMgr.runAllAnim(context);
            countAnim = 0;
        }
    }

    function gameLoop() {
        if (isStart) {
            moveUniversal();
            draw(context);
        }
    }

    let canvas = $('#game')[0];
    context = canvas.getContext("2d");
    context.fillStyle = '#000000';
    canvas.width = MAP_WIDTH;
    canvas.height = MAP_HEIGHT;

    setInterval(gameLoop, 17);

});

