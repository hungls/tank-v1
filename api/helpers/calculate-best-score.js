module.exports = {


    friendlyName: 'Calculate best score',


    description: '',


    inputs: {
        roomId: {
            type: 'string',
            required: true
        }
    },


    exits: {

        success: {
            description: 'All done.',
        },

    },


    fn: async function (inputs) {
        var bestScore = {};
        for (let i in sails.config.globals.games.bestScoreArr[inputs.roomId]) {
            let keys = Object.keys(sails.config.globals.games.bestScoreArr[inputs.roomId][i]);
            let uname = sails.config.globals.games.bestScoreArr[inputs.roomId][i][keys[0]].attackerName;
            bestScore[`${i}::${uname}`] = keys.length;
        }

        let sortedBestScore = Object.entries(bestScore)
            .sort(([, a], [, b]) => b - a)
            .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});
        return sortedBestScore;
    }


};

