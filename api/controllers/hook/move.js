module.exports = {


    friendlyName: 'Move',


    description: 'Move hook.',


    inputs: {
        user: {
            type: 'string',
            required: true
        },
        roomId: {
            type: 'string',
            required: true,
        },
        x: {
            type: 'number',
            // required: true,
        },
        y: {
            type: 'number',
            // required: true,
        },
        orient: {
            type: 'number',
            required: true
        }
    },


    exits: {

    },


    fn: async function (inputs, exits) {

        let uid = inputs.user;
        let newOrient = inputs.orient;

        let stepSize = 1;
        let numberStep = 40;

        let ctank = await sails.helpers.getTankByUid.with({ uid, roomId: inputs.roomId });
        if (!ctank) return exits.success({ message: 'Tank not found' });
        let cx = ctank.x;
        let cy = ctank.y;
        let name = ctank.name;
        let moveSteps = [{
            "uid": uid,
            "name": name,
            "y": cy,
            "x": cx,
            "orient": newOrient
        }];


        for (let ix = 0; ix < numberStep; ix++) {
            let move;
            let last = false;
            switch (newOrient) {
                case 1:
                    let newY1 = cy - (stepSize * (ix + 1));
                    if (newY1 < 20) {
                        newY1 = 20;
                        last = true
                    }

                    move = {
                        "uid": uid,
                        "name": name,
                        "y": newY1,
                        "x": cx,
                        "orient": newOrient
                    }
                    break;
                case 2:
                    let newY2 = cy + (stepSize * (ix + 1));

                    if (newY2 > (sails.config.globals.games.MAP_HEIGHT - 50)) {
                        newY2 = sails.config.globals.games.MAP_HEIGHT - 50;
                        last = true;
                    }
                    move = {
                        "uid": uid,
                        "name": name,
                        "y": newY2,
                        "x": cx,
                        "orient": newOrient
                    }
                    break;

                case 3:
                    let newX1 = cx - (stepSize * (ix + 1));
                    if (newX1 < 20) {
                        newX1 = 20;
                        last = true
                    }

                    move = {
                        "uid": uid,
                        "name": name,
                        "y": cy,
                        "x": newX1,
                        "orient": newOrient
                    }
                    break;

                case 4:
                    let newX2 = cx + (stepSize * (ix + 1));
                    if (newX2 > (sails.config.globals.games.MAP_WIDTH - 50)) {
                        newX2 = sails.config.globals.games.MAP_WIDTH - 50;
                        last = true;
                    }
                    move = {
                        "uid": uid,
                        "name": name,
                        "y": cy,
                        "x": newX2,
                        "orient": newOrient
                    }
                    break;

                default:
                    break;
            }
            if (move) moveSteps.push(move);
            if (last == true) break;
        }

        let playerTank = sails.config.globals.games.playerTank[inputs.roomId];
        for (let im = 0; im < moveSteps.length; im++) {
            let movex = moveSteps[im];
            for (let i = 0; i < playerTank.length; i++) {
                if (playerTank[i]["uid"] == uid) {
                    playerTank[i] = movex;
                }
            }

            await sails.sockets.broadcast(inputs.roomId, 'player_move', movex);
            await new Promise(r => setTimeout(r, 1));
        }

        return exits.success({ moveSteps });

    },
};
