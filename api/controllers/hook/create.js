module.exports = {


    friendlyName: 'Create',


    description: 'Create hook.',


    inputs: {
        user: {
            type: 'string',
            required: true
        },
        name: {
            type: 'string',
            required: true
        },
        roomId: {
            type: 'string',
            required: true,
        }
    },


    exits: {

    },


    fn: async function (inputs, exits) {
        let x = await sails.helpers.getRandomArbitrary.with({
            min: 40,
            max: (sails.config.globals.games.MAP_WIDTH - 100)
        });
        let y = await sails.helpers.getRandomArbitrary.with({
            min: 40,
            max: (sails.config.globals.games.MAP_HEIGHT - 100)
        });

        let tank = {
            'uid': inputs.user,
            'name': inputs.name,
            'x': x,
            'y': y,
            "orient": 1
        };

        if (!sails.config.globals.games.playerTank[inputs.roomId]) return exits.success({ message: 'Room not found' });
        const isFound = sails.config.globals.games.playerTank[inputs.roomId].some(obj => {
            return (obj.uid === tank.uid);
        });

        if (isFound) return exits.success({ message: 'On fire' });
        sails.config.globals.games.playerTank[inputs.roomId].push(tank);
        await sails.sockets.broadcast(inputs.roomId, 'new_enemy', tank);

        return exits.success({ tank });
    }


};
