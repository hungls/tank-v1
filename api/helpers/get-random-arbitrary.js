module.exports = {


    friendlyName: 'Get random arbitrary',


    description: '',


    inputs: {
        min: {
            type: 'number',
            required: true
        },
        max: {
            type: 'number',
            required: true
        }
    },


    exits: {

        success: {
            outputFriendlyName: 'Random arbitrary',
        },

    },


    fn: async function (inputs) {
        let max = (inputs.max - 100) / 40;
        let min = 2;
        let com = Math.floor(Math.random() * (max - min + 1) + min);
        return com * 40;
    }


};

